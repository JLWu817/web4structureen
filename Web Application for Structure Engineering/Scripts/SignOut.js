﻿function signOut() {
    $.ajax({
        type: 'post',
        url: '/Login/SignOut',
        data: {},
        async: false,
        success: function (data) {
            var auth2 = gapi.auth2.getAuthInstance();
            auth2.signOut().then(function () {
                $("#logoutbtn").hide();
                $("#loginbtn").show();
                $("#img").css("display", "none");
                $("#email").css("display", "none");
                auth2.disconnect();
                window.location = '/Home/Index';
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert(errorThrown);
        }
    });
}