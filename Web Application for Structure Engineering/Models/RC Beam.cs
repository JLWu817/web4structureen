﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web_Application_for_Structure_Engineering.Models
{
    public class RC_Beam
    {
        public int projectID { get; set; }
        public int accessFileID { get; set; }
        public int ID { get; set; }
        public int Mem { get; set; }
        public string Mark { get; set; }
        public string B { get; set; }
        public string T { get; set; }
        public float L { get; set; }
        public float LT { get; set; }
        public string Name { get; set; }
        public int Ct { get; set; }
        public int Cb { get; set; }
        public char CLT { get; set; }
        public char CRT { get; set; }
        public char CL { get; set; }
        public char CR { get; set; }
        public char CLH { get; set; }
        public char CRH { get; set; }
        public char AXL { get; set; }
        public char FL { get; set; }
        public char FR { get; set; }
        public int TL { get; set; }
        public int TR { get; set; }
        public int N1 { get; set; }
        public int D1 { get; set; }
        public int N2 { get; set; }
        public int D2 { get; set; }
        public int L2L { get; set; }
        public int L2R { get; set; }
        public int N3 { get; set; }
        public int D3 { get; set; }
        public int L3L { get; set; }
        public int L3R { get; set; }
        public int N4 { get; set; }
        public int D4 { get; set; }
        public int L4L { get; set; }
        public int L4R { get; set; }
        public int N5 { get; set; }
        public int D5 { get; set; }
        public int L5L { get; set; }
        public int L5R { get; set; }
        public int LN1 { get; set; }
        public int LD1 { get; set; }
        public int LN2 { get; set; }
        public int LD2 { get; set; }
        public int LL2 { get; set; }
        public int LN3 { get; set; }
        public int LD3 { get; set; }
        public int LL3 { get; set; }
        public int LN4 { get; set; }
        public int LD4 { get; set; }
        public int LL4 { get; set; }
        public int LN5 { get; set; }
        public int LD5 { get; set; }
        public int LL5 { get; set; }
        public int RN2 { get; set; }
        public int RD2 { get; set; }
        public int RL2 { get; set; }
        public int RN3 { get; set; }
        public int RD3 { get; set; }
        public int RL3 { get; set; }
        public int RN4 { get; set; }
        public int RD4 { get; set; }
        public int RL4 { get; set; }
        public int RN5 { get; set; }
        public int RD5 { get; set; }
        public int RL5 { get; set; }
        public int N11 { get; set; }
        public int D11 { get; set; }
        public int RS2 { get; set; }
        public char Cot2 { get; set; }
        public char Cot3 { get; set; }
        public char Cot4 { get; set; }
        public char Cot5 { get; set; }
        public char Cob2 { get; set; }
        public char Cob3 { get; set; }
        public char Cob4 { get; set; }
        public char Cob5 { get; set; }
        public int Ds { get; set; }
        public int Nso { get; set; }
        public int Nsi { get; set; }
        public int Nslo { get; set; }
        public int Nsli { get; set; }
        public int Nsro { get; set; }
        public int Nsri { get; set; }
        public int Stln { get; set; }
        public int Stls { get; set; }
        public int Sts { get; set; }
        public int Strn { get; set; }
        public int Strs { get; set; }
        public string MEMLIST { get; set; }

        public bool IsCheck { get; set; }
        public bool IsCheck2 { get; set; }
        public bool IsCheck3 { get; set; }
        public bool IsCheck4 { get; set; }
        public bool IsCheck5 { get; set; }
        public bool IsCheck6 { get; set; }
        public bool IsCheck7 { get; set; }
        public bool IsCheck8 { get; set; }
        public bool IsCheck9 { get; set; }
        public bool IsCheck10 { get; set; }
        public bool checkDesignAll { get; set; }
        public float SX0 { get; set; }
        public float BX0 { get; set; }
        public float AX0 { get; set; }
        public string CaseX0 { get; set; }
        public float M3X0 { get; set; }
        public float V2X0 { get; set; }
        public float TX0 { get; set; }
        public float PX0 { get; set; }
        public float SX1 { get; set; }
        public string StaX1 { get; set; }
        public float BX1 { get; set; }
        public float AX1 { get; set; }
        public string CaseX1 { get; set; }
        public float M3X1 { get; set; }
        public float V2X1 { get; set; }
        public float TX1 { get; set; }
        public float PX1 { get; set; }
        public float SX2 { get; set; }
        public string StaX2 { get; set; }
        public float BX2 { get; set; }
        public float AX2 { get; set; }
        public string CaseX2 { get; set; }
        public float M3X2 { get; set; }
        public float V2X2 { get; set; }
        public float TX2 { get; set; }
        public float PX2 { get; set; }
        public float SXM { get; set; }
        public float BXM { get; set; }
        public float AXM { get; set; }
        public string CaseXM { get; set; }
        public float M3XM { get; set; }
        public float V2XM { get; set; }
        public float TXM { get; set; }
        public float PXM { get; set; }
        public float SX8 { get; set; }
        public string StaX8 { get; set; }
        public float BX8 { get; set; }
        public float AX8 { get; set; }
        public string CaseX8 { get; set; }
        public float M3X8 { get; set; }
        public float V2X8 { get; set; }
        public float TX8 { get; set; }
        public float PX8 { get; set; }
        public float SX9 { get; set; }
        public string StaX9 { get; set; }
        public float BX9 { get; set; }
        public float AX9 { get; set; }
        public string CaseX9 { get; set; }
        public float M3X9 { get; set; }
        public float V2X9 { get; set; }
        public float TX9 { get; set; }
        public float PX9 { get; set; }
        public float SX10 { get; set; }
        public float BX10 { get; set; }
        public float AX10 { get; set; }
        public string CaseX10 { get; set; }
        public float M3X10 { get; set; }
        public float V2X10 { get; set; }
        public float TX10 { get; set; }
        public float PX10 { get; set; }
        public float XV0 { get; set; }
        public float XT0 { get; set; }
        public float XVc0 { get; set; }
        public string CaseXV0 { get; set; }
        public float M3XV0 { get; set; }
        public float V2XV0 { get; set; }
        public float TXV0 { get; set; }
        public float PXV0 { get; set; }
        public float XV1 { get; set; }
        public float XT1 { get; set; }
        public float XVc1 { get; set; }
        public string CaseXV1 { get; set; }
        public float M3XV1 { get; set; }
        public float V2XV1 { get; set; }
        public float TXV1 { get; set; }
        public float PXV1 { get; set; }
        public float XV9 { get; set; }
        public float XT9 { get; set; }
        public float XVc9 { get; set; }
        public string CaseXV9 { get; set; }
        public float M3XV9 { get; set; }
        public float V2XV9 { get; set; }
        public float TXV9 { get; set; }
        public float PXV9 { get; set; }
        public float XV10 { get; set; }
        public float XT10 { get; set; }
        public float XVc10 { get; set; }
        public string CaseXV10 { get; set; }
        public float M3XV10 { get; set; }
        public float V2XV10 { get; set; }
        public float TXV10 { get; set; }
        public float PXV10 { get; set; }
        public float SXMax { get; set; }
        public float BXMax { get; set; }
        public float AXMax { get; set; }
        public float SMaxSta { get; set; }
        public float M3XMax { get; set; }
        public float V2XMax { get; set; }
        public float TXMax { get; set; }
        public float PXMax { get; set; }
        public float XVMax { get; set; }
        public float XTMax { get; set; }
        public float XVcMax { get; set; }
        public float VMaxSta { get; set; }
        public float M3XVMax { get; set; }
        public float V2XVMax { get; set; }
        public float TXVMax { get; set; }
        public float PXVMax { get; set; }
        public char BL { get; set; }
        public char BR { get; set; }
        public char AsMin { get; set; }

        public List<char> CLTList = new List<char> { 'F', 'T' };
        public List<char> CRTList = new List<char> { 'F', 'T' };
        public List<char> CLHList = new List<char> { 'F', 'T' };
        public List<char> AXLList = new List<char> { 'F', 'T' };
        public List<char> CRList = new List<char> { 'F', 'T' };
        public List<char> CRHList = new List<char> { 'F', 'T' };
        public List<char> CLList = new List<char> { 'F', 'T' };

        public List<Object> LD1List = new List<Object> { new { value = 0, text = " " }, new { value = 12, text = "12" }, new { value = 16, text = "16" }, new { value = 20, text = "20" }, new { value = 25, text = "25" }, new { value = 28, text = "28" }, new { value = 32, text = "32" } };

        //public List<Object> LD1List = new List<Object> { new { value = 0, text = " " }, new { value = 12, text = "12" }, new { value = 16, text = "16" }, new { value = 20, text = "20" }, new { value = 25, text = "25" }, new { value = 28, text = "28" }, new { value = 32, text = "32" } };
        public List<Object> LD2List = new List<Object> { new { value = 0, text = " " }, new { value = 12, text = "12" }, new { value = 16, text = "16" }, new { value = 20, text = "20" }, new { value = 25, text = "25" }, new { value = 28, text = "28" }, new { value = 32, text = "32" } };
        public List<Object> LD3List = new List<Object> { new { value = 0, text = " " }, new { value = 12, text = "12" }, new { value = 16, text = "16" }, new { value = 20, text = "20" }, new { value = 25, text = "25" }, new { value = 28, text = "28" }, new { value = 32, text = "32" } };
        public List<Object> LD4List = new List<Object> { new { value = 0, text = " " }, new { value = 12, text = "12" }, new { value = 16, text = "16" }, new { value = 20, text = "20" }, new { value = 25, text = "25" }, new { value = 28, text = "28" }, new { value = 32, text = "32" } };
        public List<Object> LD5List = new List<Object> { new { value = 0, text = " " }, new { value = 12, text = "12" }, new { value = 16, text = "16" }, new { value = 20, text = "20" }, new { value = 25, text = "25" }, new { value = 28, text = "28" }, new { value = 32, text = "32" } };

        public List<Object> RD2List = new List<Object> { new { value = 0, text = " " }, new { value = 12, text = "12" }, new { value = 16, text = "16" }, new { value = 20, text = "20" }, new { value = 25, text = "25" }, new { value = 28, text = "28" }, new { value = 32, text = "32" } };
        public List<Object> RD3List = new List<Object> { new { value = 0, text = " " }, new { value = 12, text = "12" }, new { value = 16, text = "16" }, new { value = 20, text = "20" }, new { value = 25, text = "25" }, new { value = 28, text = "28" }, new { value = 32, text = "32" } };
        public List<Object> RD4List = new List<Object> { new { value = 0, text = " " }, new { value = 12, text = "12" }, new { value = 16, text = "16" }, new { value = 20, text = "20" }, new { value = 25, text = "25" }, new { value = 28, text = "28" }, new { value = 32, text = "32" } };
        public List<Object> RD5List = new List<Object> { new { value = 0, text = " " }, new { value = 12, text = "12" }, new { value = 16, text = "16" }, new { value = 20, text = "20" }, new { value = 25, text = "25" }, new { value = 28, text = "28" }, new { value = 32, text = "32" } };

        public List<Object> D1List = new List<Object> { new { value = 0, text = " " }, new { value = 12, text = "12" }, new { value = 16, text = "16" }, new { value = 20, text = "20" }, new { value = 25, text = "25" }, new { value = 28, text = "28" }, new { value = 32, text = "32" } };
        public List<Object> D2List = new List<Object> { new { value = 0, text = " " }, new { value = 12, text = "12" }, new { value = 16, text = "16" }, new { value = 20, text = "20" }, new { value = 25, text = "25" }, new { value = 28, text = "28" }, new { value = 32, text = "32" } };
        public List<Object> D3List = new List<Object> { new { value = 0, text = " " }, new { value = 12, text = "12" }, new { value = 16, text = "16" }, new { value = 20, text = "20" }, new { value = 25, text = "25" }, new { value = 28, text = "28" }, new { value = 32, text = "32" } };
        public List<Object> D4List = new List<Object> { new { value = 0, text = " " }, new { value = 12, text = "12" }, new { value = 16, text = "16" }, new { value = 20, text = "20" }, new { value = 25, text = "25" }, new { value = 28, text = "28" }, new { value = 32, text = "32" } };
        public List<Object> D5List = new List<Object> { new { value = 0, text = " " }, new { value = 12, text = "12" }, new { value = 16, text = "16" }, new { value = 20, text = "20" }, new { value = 25, text = "25" }, new { value = 28, text = "28" }, new { value = 32, text = "32" } };

        public List<Object> D11List = new List<Object> { new { value = 0, text = " " }, new { value = 10, text = "10" }, new { value = 12, text = "12" }, new { value = 16, text = "16" }, new { value = 20, text = "20" }, new { value = 25, text = "25" }, new { value = 28, text = "28" }, new { value = 32, text = "32" } };
        public List<Object> DsList = new List<Object> { new { value = 0, text = " " }, new { value = 6, text = "6" }, new { value = 9, text = "9" }, new { value = 10, text = "10" }, new { value = 12, text = "12" } };

        public bool CONSIDERAXIAL { get; set; }
        public bool DETAILEDCALCULATION { get; set; }

    }
}