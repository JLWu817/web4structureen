﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web_Application_for_Structure_Engineering.Models
{
    public class Element_Forces___Frames
    {
        public int projectID { get; set; }
        public int accessFileID { get; set; }
        public int EFFID { get; set; }
        public int Frame { get; set; }
        public float Station { get; set; }
        public string OutputCase { get; set; }
        public string CaseType { get; set; }
        public float P { get; set; }
        public float V2 { get; set; }
        public float V3 { get; set; }
        public float T { get; set; }
        public float M2 { get; set; }
        public float M3 { get; set; }
    }
}