﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web_Application_for_Structure_Engineering.Models
{
    public class FindMomentRatio
    {
        public double? Z { get; set; }
        public double? Es1 { get; set; }
        public double? Ccm { get; set; }
        public double? Fs1plus2ksc { get; set; }
        public double? Es3 { get; set; }
        public double? Fs3ksc { get; set; }
        public double? Es4 { get; set; }
        public double? Fs4ksc { get; set; }
        public double? Es5 { get; set; }
        public double? Fs5ksc { get; set; }
        public double? EsPrime1 { get; set; }
        public double? FsPrime1plus2ksc { get; set; }
        public double? EsPrime3 { get; set; }
        public double? FsPrime3ksc { get; set; }
        public double? EsPrime4 { get; set; }
        public double? FsPrime4ksc { get; set; }
        public double? EsPrime5 { get; set; }
        public double? FsPrime5ksc { get; set; }
        public double? acm { get; set; }
        public double? Ckg { get; set; }
        public double? fs1plus2kg { get; set; }
        public double? Fs3kg { get; set; }
        public double? Fs4kg { get; set; }
        public double? Fs5kg { get; set; }
        public double? SFsikg { get; set; }
        public double? FsPrime1plus2kg { get; set; }
        public double? FsPrime3kg { get; set; }
        public double? FsPrime4kg { get; set; }
        public double? FsPrime5kg { get; set; }
        public double? SFsPrimekg { get; set; }
        public double? Cha { get; set; }
        public double? SFsiHDi { get; set; }
        public double? SFsPrimei { get; set; }
        public double? Pnkg { get; set; }
        public double? Mnkgm { get; set; }
        public double? Ceta { get; set; }
        public double? CetaPnMaxKg { get; set; }
        public double? CetaPnkg { get; set; }
        public double? CetaMnkgm { get; set; }
        public double? M { get; set; }
        public double? DIFFm { get; set; }
        public string CONDITION1 { get; set; }
        public string GREATERm { get; set; }
        public string LOWERm { get; set; }
        public double? CetaPnkgOFLEFTPOINT { get; set; }
        public double? CetaMnkgOFLEFTPOINT { get; set; }
        public double? CetaPnkgOFRIGHTPOINT { get; set; }
        public double? CetaMnkgOFRIGHTPOINT { get; set; }
        public string CONDITION2 { get; set; }
        public string POF1stNEARESTPOINT { get; set; }
        public string MOF1stNEARESTPOINT { get; set; }
        public string POF2ndNEARESTPOINT { get; set; }
        public string MOF2ndNEARESTPOINT { get; set; }
    }
}