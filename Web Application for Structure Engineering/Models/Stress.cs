﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web_Application_for_Structure_Engineering.Models
{
    public class Stress
    {
        public int Id { get; set; }
        public int MEM { get; set; }
        public float Station { get; set; }
        public string Secc { get; set; }
        public string Case { get; set; }
        public float M3 { get; set; }
        public float V2 { get; set; }
        public float T { get; set; }
        public float P { get; set; }
        public int N1c { get; set; }
        public int D1c { get; set; }
        public float Ld1c { get; set; }
        public int N2c { get; set; }
        public int D2c { get; set; }
        public float Ld2c { get; set; }
        public int N3c { get; set; }
        public int D3c { get; set; }
        public float Ld3c { get; set; }
        public int N4c { get; set; }
        public int D4c { get; set; }
        public float Ld4c { get; set; }
        public int N5c { get; set; }
        public int D5c { get; set; }
        public float Ld5c { get; set; }
        public int N1t { get; set; }
        public int D1t { get; set; }
        public float Ld1t { get; set; }
        public int N2t { get; set; }
        public int D2t { get; set; }
        public float Ld2t { get; set; }
        public int N3t { get; set; }
        public int D3t { get; set; }
        public float Ld3t { get; set; }
        public int N4t { get; set; }
        public int D4t { get; set; }
        public float Ld4t { get; set; }
        public int N5t { get; set; }
        public int D5t { get; set; }
        public float Ld5t { get; set; }
        public int N11 { get; set; }
        public int D11 { get; set; }
        public float Ld11 { get; set; }
        public int Ds { get; set; }
        public int Nsout { get; set; }
        public int Nsin { get; set; }
        public int S { get; set; }
        public char Asmin { get; set; }
        public float Strength { get; set; }
        public float Bending { get; set; }
        public float Axial { get; set; }
        public float Torsion { get; set; }
        public float Shear { get; set; }
        public string Vconc { get; set; }
        public char Mm { get; set; }
        public char BL { get; set; }
        public char BR { get; set; }
        public float Al { get; set; }
        public string Avs { get; set; }
        public float Ats { get; set; }
        public float AvtSoutprovide { get; set; }
        public float AvSinprovide { get; set; }
        public float AvSallprovide { get; set; }
        public float d { get; set; }
        public float FINALEACHEDGEBOT1 { get; set; }
        public float FINALEACHEDGEBOT2 { get; set; }
        public float FINALEACHEDGEBOT3 { get; set; }
        public float FINALEACHEDGEBOT4 { get; set; }
        public float FINALEACHEDGEBOT5 { get; set; }
        public float FINALEACHEDGETOP1 { get; set; }
        public float FINALEACHEDGETOP2 { get; set; }
        public float FINALEACHEDGETOP3 { get; set; }
        public float FINALEACHEDGETOP4 { get; set; }
        public float FINALEACHEDGETOP5 { get; set; }
    }
}