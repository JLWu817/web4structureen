﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web_Application_for_Structure_Engineering.Models
{
    public class Strength
    {
        public float fcp { get; set; }
        public float fy { get; set; }
        public float fvyr { get; set; }
        public float fvyd { get; set; }
        public float es { get; set; }
        public decimal ec { get; set; }
        public decimal utf { get; set; }
    }
}